package handler

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/jamesvincentsiauw/fizzbuzz-go-rest-api/service"
)

const (
	maxRange = 100
)

type FizzBuzzResponse struct {
	Status string `json:"status"`
	Data   string `json:"data"`
}

type ErrorResponse struct {
	Error string `json:"error"`
}

type FizzBuzzHandler struct {
	fizzBuzzService service.FizzBuzzService
}

func NewFizzBuzzHandler(fizzBuzzService service.FizzBuzzService) *FizzBuzzHandler {
	return &FizzBuzzHandler{
		fizzBuzzService: fizzBuzzService,
	}
}

// handler range fizzbuzz
func (h *FizzBuzzHandler) RangeFizzBuzz(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	fromStr := r.URL.Query().Get("from")
	toStr := r.URL.Query().Get("to")

	from, errFrom := strconv.Atoi(fromStr)
	to, errTo := strconv.Atoi(toStr)

	if errFrom != nil || errTo != nil || from > to {
		w.WriteHeader(http.StatusBadRequest)
		response := ErrorResponse{Error: "Invalid 'from' or 'to' parameters"}
		json.NewEncoder(w).Encode(response)
		return
	}
	if to-from > maxRange {
		w.WriteHeader(http.StatusBadRequest)
		response := ErrorResponse{Error: "Maximum range is 100 number"}
		json.NewEncoder(w).Encode(response)
		return
	}

	payload := service.FizzBuzzPayload{
		From: from,
		To:   to,
	}
	result := h.fizzBuzzService.RangeFizzBuzz(payload)

	w.WriteHeader(http.StatusOK)
	response := FizzBuzzResponse{
		Status: "success",
		Data:   result,
	}

	json.NewEncoder(w).Encode(response)
}
