package handler

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/stretchr/testify/mock"
	mocks "gitlab.com/jamesvincentsiauw/fizzbuzz-go-rest-api/mocks/service"
	"gitlab.com/jamesvincentsiauw/fizzbuzz-go-rest-api/service"
)

func TestNewFizzBuzzHandler(t *testing.T) {
	mockFizzBuzzService := mocks.NewFizzBuzzService(t)
	type args struct {
		fizzBuzzService service.FizzBuzzService
	}
	tests := []struct {
		name string
		args args
		want *FizzBuzzHandler
	}{
		{
			name: "SUCCESS",
			args: args{
				fizzBuzzService: mockFizzBuzzService,
			},
			want: &FizzBuzzHandler{
				fizzBuzzService: mockFizzBuzzService,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewFizzBuzzHandler(tt.args.fizzBuzzService); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewFizzBuzzHandler() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFizzBuzzHandler_RangeFizzBuzz(t *testing.T) {
	type args struct {
		w http.ResponseWriter
		r *http.Request
	}
	tests := []struct {
		name       string
		beforeTest func(mockFizzbuzzService *mocks.FizzBuzzService)
		args       args
	}{
		{
			name: "SUCCESS",
			beforeTest: func(mockFizzbuzzService *mocks.FizzBuzzService) {
				mockFizzbuzzService.On("RangeFizzBuzz", mock.AnythingOfType("service.FizzBuzzPayload")).Return("1 2")
			},
			args: args{
				w: httptest.NewRecorder(),
				r: httptest.NewRequest("GET", "/range-fizzbuzz?from=1&to=2", nil),
			},
		},
		{
			name: "STATUS_BAD_REQUEST_QUERY_PARAMS_MISSING",
			beforeTest: func(mockFizzbuzzService *mocks.FizzBuzzService) {
			},
			args: args{
				w: httptest.NewRecorder(),
				r: httptest.NewRequest("GET", "/range-fizzbuzz?from=1", nil),
			},
		},
		{
			name: "STATUS_BAD_REQUEST_MAX_RANGE_EXCEEDED",
			beforeTest: func(mockFizzbuzzService *mocks.FizzBuzzService) {
			},
			args: args{
				w: httptest.NewRecorder(),
				r: httptest.NewRequest("GET", "/range-fizzbuzz?from=1&to=1000", nil),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockFizzBuzzService := mocks.NewFizzBuzzService(t)
			tt.beforeTest(mockFizzBuzzService)
			h := &FizzBuzzHandler{
				fizzBuzzService: mockFizzBuzzService,
			}
			h.RangeFizzBuzz(tt.args.w, tt.args.r)
		})
	}
}
