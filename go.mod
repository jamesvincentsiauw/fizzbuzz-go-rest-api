module gitlab.com/jamesvincentsiauw/fizzbuzz-go-rest-api

go 1.21.0

require github.com/rs/zerolog v1.33.0

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.5.2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/stretchr/testify v1.9.0
	golang.org/x/sys v0.12.0 // indirect
)
