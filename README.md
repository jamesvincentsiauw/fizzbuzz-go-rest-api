# FizzBuzz Go REST API

This is a simple Go REST API that implements the FizzBuzz game.

## Prerequisites

Before running this application, make sure you have the following installed:

- Go (version 1.21.0)
- [Mux](https://github.com/gorilla/mux) (Go web framework)

## Installation

1. Clone this repository:

    ```shell
    $ git clone https://github.com/your-username/fizzbuzz-go-rest-api.git
    ```

2. Change to the project directory:

    ```shell
    $ cd fizzbuzz-go-rest-api
    ```

3. Install the required dependencies:

    ```shell
    $ go mod download
    ```

## Usage

To run the FizzBuzz Go REST API, use the following command:

1. Use the build binary:

    ```shell
    $ make build
    $ ./build/fizzbuzz-go-rest-api
    ```

2. Use the docker:

    ```shell
    $ make docker-build
    $ make docker-run
    ```

The application will be live on port 8080