# Makefile for FizzBuzz Go REST API

# Variables
GO := go
BINARY_NAME := fizzbuzz-go-rest-api
BUILD_DIR := build

# Default target
all: build

# Build target
build:
	@mkdir -p $(BUILD_DIR)
	$(GO) build -o $(BUILD_DIR)/$(BINARY_NAME)

# Clean target
clean:
	@rm -rf $(BUILD_DIR)

# Run target
run:
	$(GO) run .

# Install dependencies target
deps:
	$(GO) mod download

# Test target
test:
	$(GO) test -v ./...

# Help target
help:
	@echo "Available targets:"
	@echo "  make build     - Build the application"
	@echo "  make clean     - Clean the build directory"
	@echo "  make run       - Run the application"
	@echo "  make deps      - Install dependencies"
	@echo "  make test      - Run tests"
	@echo "  make help      - Show this help message"

# Docker target
docker-build:
	docker build -t fizzbuzz-go-rest-api .

docker-run:
	docker run --name fizzbuzz-go-rest-api -p 8080:8080 fizzbuzz-go-rest-api

.PHONY: all build clean run deps test help