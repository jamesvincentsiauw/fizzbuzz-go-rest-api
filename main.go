package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/jamesvincentsiauw/fizzbuzz-go-rest-api/handler"
	"gitlab.com/jamesvincentsiauw/fizzbuzz-go-rest-api/middleware"
	service "gitlab.com/jamesvincentsiauw/fizzbuzz-go-rest-api/service/impl"
)

func main() {
	// initialize zlogger
	zlogger := zerolog.New(os.Stdout).With().Timestamp().Logger()

	// initialize middlewares
	logger := middleware.NewLogger(&zlogger)

	// initialize services
	fizbuzzService := service.NewFizzBuzzService()

	// initialize the handlers
	fizzBuzzHandler := handler.NewFizzBuzzHandler(fizbuzzService)

	// register handlers
	mux := http.NewServeMux()
	mux.HandleFunc("/range-fizzbuzz", fizzBuzzHandler.RangeFizzBuzz)
	loggedMux := logger.LoggingMiddleware(mux)

	server := &http.Server{
		Addr:    ":8080",
		Handler: loggedMux,
	}

	// Graceful shutdown handling
	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
		<-sigint

		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		if err := server.Shutdown(ctx); err != nil {
			zlogger.Error().Err(err).Msg("HTTP server Shutdown")
		}
		close(idleConnsClosed)
	}()

	zlogger.Info().Msg("Starting server on :8080")
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		zlogger.Fatal().Err(err).Msg("HTTP server ListenAndServe")
	}

	<-idleConnsClosed
	zlogger.Info().Msg("Server stopped gracefully")
}
