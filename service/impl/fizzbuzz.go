package service

import (
	"strconv"
	"strings"
	"sync"

	"gitlab.com/jamesvincentsiauw/fizzbuzz-go-rest-api/service"
)

const (
	maxConcurrentWorkers = 1000
)

type fizzBuzzService struct{}
type FizzBuzzResult struct {
	Index int
	Value string
}

func NewFizzBuzzService() service.FizzBuzzService {
	return &fizzBuzzService{}
}

func worker(jobs <-chan FizzBuzzResult, results chan<- FizzBuzzResult, wg *sync.WaitGroup) {
	defer wg.Done()
	for job := range jobs {
		job.Value = singleFizzBuzz(job.Index)
		results <- job
	}
}

func singleFizzBuzz(n int) string {
	switch {
	case n%3 == 0 && n%5 == 0:
		return "FizzBuzz"
	case n%3 == 0:
		return "Fizz"
	case n%5 == 0:
		return "Buzz"
	default:
		return strconv.Itoa(n)
	}
}

func (f *fizzBuzzService) RangeFizzBuzz(payload service.FizzBuzzPayload) string {
	from := payload.From
	to := payload.To

	numJobs := to - from + 1
	jobs := make(chan FizzBuzzResult, numJobs)
	results := make(chan FizzBuzzResult, numJobs)

	var wg sync.WaitGroup
	for i := 0; i < maxConcurrentWorkers; i++ {
		wg.Add(1)
		go worker(jobs, results, &wg)
	}

	go func() {
		for i := from; i <= to; i++ {
			jobs <- FizzBuzzResult{Index: i}
		}
		close(jobs)
	}()

	go func() {
		wg.Wait()
		close(results)
	}()

	// order the results
	resultStrings := make([]string, numJobs)
	for result := range results {
		resultStrings[result.Index-from] = result.Value
	}

	return strings.Join(resultStrings, " ")
}
