package service

import (
	"reflect"
	"testing"

	"gitlab.com/jamesvincentsiauw/fizzbuzz-go-rest-api/service"
)

func TestNewFizzBuzzService(t *testing.T) {
	tests := []struct {
		name string
		want service.FizzBuzzService
	}{
		{
			name: "SUCCESS",
			want: &fizzBuzzService{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewFizzBuzzService(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewFizzBuzzService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_fizzBuzzService_singleFizzBuzz(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		f    *fizzBuzzService
		args args
		want string
	}{
		{
			name: "SUCCESS_FIZZBUZZ",
			f:    &fizzBuzzService{},
			args: args{
				n: 15,
			},
			want: "FizzBuzz",
		},
		{
			name: "SUCCESS_FIZZ",
			f:    &fizzBuzzService{},
			args: args{
				n: 3,
			},
			want: "Fizz",
		},
		{
			name: "SUCCESS_BUZZ",
			f:    &fizzBuzzService{},
			args: args{
				n: 5,
			},
			want: "Buzz",
		},
		{
			name: "SUCCESS_NUMBER",
			f:    &fizzBuzzService{},
			args: args{
				n: 1,
			},
			want: "1",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := singleFizzBuzz(tt.args.n); got != tt.want {
				t.Errorf("singleFizzBuzz() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_fizzBuzzService_RangeFizzBuzz(t *testing.T) {
	type args struct {
		payload service.FizzBuzzPayload
	}
	tests := []struct {
		name string
		f    *fizzBuzzService
		args args
		want string
	}{
		{
			name: "SUCCESS",
			f:    &fizzBuzzService{},
			args: args{
				payload: service.FizzBuzzPayload{
					From: 1,
					To:   2,
				},
			},
			want: "1 2",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &fizzBuzzService{}
			if got := f.RangeFizzBuzz(tt.args.payload); got != tt.want {
				t.Errorf("fizzBuzzService.RangeFizzBuzz() = %v, want %v", got, tt.want)
			}
		})
	}
}
