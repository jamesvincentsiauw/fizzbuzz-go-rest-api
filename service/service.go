package service

type FizzBuzzPayload struct {
	From int `json:"from"`
	To   int `json:"to"`
}

type FizzBuzzService interface {
	RangeFizzBuzz(payload FizzBuzzPayload) string
}
