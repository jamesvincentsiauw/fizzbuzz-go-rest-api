package middleware

import (
	"bytes"
	"net/http"
	"time"

	"github.com/rs/zerolog"
)

type Logger struct {
	zlogger *zerolog.Logger
}

func NewLogger(zlogger *zerolog.Logger) *Logger {
	return &Logger{
		zlogger: zlogger,
	}
}

type responseRecorder struct {
	http.ResponseWriter
	statusCode int
	body       *bytes.Buffer
}

// LoggingMiddleware is a middleware that logs the request
func (l *Logger) LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		rec := &responseRecorder{ResponseWriter: w, statusCode: http.StatusOK, body: &bytes.Buffer{}}
		next.ServeHTTP(rec, r)

		duration := time.Since(start)

		if rec.statusCode == 200 {
			l.zlogger.Info().
				Str("method", r.Method).
				Str("url", r.URL.String()).
				Int("status", rec.statusCode).
				Dur("duration", duration).
				Str("response", rec.body.String()).
				Msg("Request processed")
		} else {
			l.zlogger.Error().
				Str("method", r.Method).
				Str("url", r.URL.String()).
				Int("status", rec.statusCode).
				Dur("duration", duration).
				Str("response", rec.body.String()).
				Msg("Request processed")
		}
	})
}

func (rec *responseRecorder) WriteHeader(code int) {
	rec.statusCode = code
	rec.ResponseWriter.WriteHeader(code)
}

func (rec *responseRecorder) Write(b []byte) (int, error) {
	rec.body.Write(b)
	return rec.ResponseWriter.Write(b)
}
